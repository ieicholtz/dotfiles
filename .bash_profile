PATH=$HOME/bin:/usr/local/zend/bin:/usr/local/zend/mysql/bin:$PATH
export PATH

alias docs="cd /usr/local/zend/apache2/htdocs"
alias extra="cd /usr/local/zend/apache2/conf/extra"

alias push="cd /wp-content/themes/push"
alias wgh="cd /usr/local/zend/apache2/htdocs/World-Golf"
alias tjs="cd /usr/local/zend/apache2/htdocs/Tijuana-Flats3"
alias kap="cd /usr/local/zend/apache2/htdocs/kapstone-paper"
alias mpr="cd /usr/local/zend/apache2/htdocs/MaidPro"
alias pit="cd /usr/local/zend/apache2/htdocs/SON-Pitmaster-Challenge"
alias son="cd /usr/local/zend/apache2/htdocs/SonnysBBQ"
alias ins="cd /usr/local/zend/apache2/htdocs/Intelligent-InSites"
alias pgd="cd /usr/local/zend/apache2/htdocs/PGD-Rewards"
alias crk="cd /usr/local/zend/apache2/htdocs/Corkcicle-Social"
alias tgl="cd /usr/local/zend/apache2/htdocs/Giving-Library"
alias rab="cd /usr/local/zend/apache2/htdocs/Random-Acts-Of-BBQ"
alias dash="cd /usr/local/zend/apache2/htdocs/matrix-dashboard"
alias api="cd /usr/local/zend/apache2/htdocs/matrix-api"
alias hc="cd /usr/local/zend/apache2/htdocs/matrix-hc"
alias hc2="cd /usr/local/zend/apache2/htdocs/matrix-hc-v2"

alias comm="git commit -a -m"

