" Initialization
" ------------------------------------------------------------------------- 
set nocompatible
let mapleader = ","
filetype plugin on
syntax on

" Pathogen (as a bundle)
" ------------------------------------------------------------------------- 
runtime bundle/vim-pathogen/autoload/pathogen.vim
silent! call pathogen#infect()
    Helptags
"Ensure Clean Pasting w/autoindented code
nnoremap <F2> :set invpaste paste?<CR>
set pastetoggle=<F2> 

" Shortcuts
" ------------------------------------------------------------------------- 

" Jump to Current working directory
nmap <Leader>. :e .<CR>

" Jump to Current working directory
nmap - :Explore<CR>

" Toggle hidden chars
set listchars=tab:·\ 
set listchars+=extends:…
nmap <Leader>l :set list!<CR>

" Toggle line wrapping
nmap <Leader>w :set wrap!<CR>

" Toggle line numbers
set nu
nmap <Leader>n :set nu!<CR>

" .vimrc Helpers
nmap <Leader>er :tabnew ~/.vimrc<CR>
nmap <Leader>sr :source ~/.vimrc<CR>


" Indentation
" ------------------------------------------------------------------------- 

set shiftwidth=4
set softtabstop=4
set tabstop=4
set expandtab
set autoindent
set cindent
set indentexpr=

colorscheme murphy
